function [ entropy ] = EntropyLogOdds( log_odds )
%ENTROPYLOGODDS Summary of this function goes here
%   Detailed explanation goes here
[p, p_] = logodds2prob(log_odds);
entropy = calculate_entropy_bernoulli( p );
end

