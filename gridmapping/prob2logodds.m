function [ log_odds ] = prob2logodds( p )
%PROB2LOGODDS Summary of this function goes here
%   Detailed explanation goes here
p(p<0.0000001) = 0.0000001;
p(p>0.999999) = 0.999999;
log_odds = log(p./(1-p));
end

