function [ p_i, p_u ] = GetMeasurementProbability(meas,range)
    p_ii = GetInterestingGivenInterestingProbability(range);
    p_ui = 1-p_ii;
    p_uu = GetUnInterestinGivenUninterestingProbability(range);
    p_iu = 1-p_uu;
    
    p_i = ones(size(meas,1),1);
    p_u = ones(size(meas,1),1);
    
    p_i(meas==1) = p_ii(meas==1);
    p_i(meas==0) = p_ui(meas==0);
    
    p_u(meas==1) = p_iu(meas==1);
    p_u(meas==0) = p_uu(meas==0);
end