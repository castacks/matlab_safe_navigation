function [ p ] = GetInterestingGivenInterestingProbability( range )
%GetInterestingGivenInterestingProbability p(z=interesting|Interesting)
%   Detailed explanation goes here
x = [0,10,10.1,20,40,60,100,1000000];
y = [0.5,0.5,1,1,0.5,0.5,0.5,0.5];

p = interp1(x,y,range,'linear','extrap');
end

