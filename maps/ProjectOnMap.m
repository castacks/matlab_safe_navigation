function [ ID, ID_changed ] = ProjectOnMap(ID,size_map)
%PROJECTONMAP Summary of this function goes here
%   Detailed explanation goes here
ID_changed = find(ID(:,1)<1 | ID(:,2)<1 | ID(:,1)>size_map(1) | ID(:,2)>size_map(2));
ID(ID(:,1)<1,1) = 1;
ID(ID(:,2)<1,2) = 1;

ID(ID(:,1)>size_map(1),1) = size_map(1);
ID(ID(:,2)>size_map(2),2) = size_map(2);
end

