function [values, valid] = GetMapValues(coords, map, map_struct)
    [ ID, valid ] = getValidIds( coords, map,map_struct);
    values = NaN*zeros(size(ID,1),1);
    ID = ID(valid>0,:);
    IND = sub2ind(size(map), ID(:,1), ID(:,2));
    values(valid>0) = map(IND);    
end