function [ globalMap ] = findLocal2GlobalTransformedMap(minX,minY,maxX,maxY,resolution,localMap,translation,rotationAngle,bigMapResolution,bigMapMinX,bigMapMinY,bigMap)

localExtrema = [minX,minY;minX,maxY;maxX,maxY;maxX,minY;minX,minY];
globalExtrema = transformPoint2Global(localExtrema,rotationAngle,translation);
globalExtrema = [min(globalExtrema);max(globalExtrema)];
[X,Y] = meshgrid(globalExtrema(1,1):resolution:globalExtrema(2,1),globalExtrema(1,2):resolution:globalExtrema(2,2));
globalCoordinates = [X(:),Y(:)];

localCoordinates = transformPoint2local(globalCoordinates,rotationAngle,translation);
globalCoordinates(localCoordinates(:,1)>maxX,:) = [];
localCoordinates(localCoordinates(:,1)>maxX,:) = [];
globalCoordinates(localCoordinates(:,1)<minX,:) = [];
localCoordinates(localCoordinates(:,1)<minX,:) = [];
globalCoordinates(localCoordinates(:,2)<minY,:) = [];
localCoordinates(localCoordinates(:,2)<minY,:) = [];
globalCoordinates(localCoordinates(:,2)>maxY,:) = [];
localCoordinates(localCoordinates(:,2)>maxY,:) = [];

globalCoordinates = coord2index(globalCoordinates(:,1),globalCoordinates(:,2),bigMapResolution,bigMapMinX,bigMapMinY);
localCoordinates = coord2index(localCoordinates(:,1),localCoordinates(:,2),resolution,minX,minY);
localInd = sub2ind(size(localMap), localCoordinates(:,1), localCoordinates(:,2));
globalInd = sub2ind(size(bigMap), globalCoordinates(:,1), globalCoordinates(:,2));

bigMap(globalInd(:)) = localMap(localInd(:));
globalMap = bigMap;
end

