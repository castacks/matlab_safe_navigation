clear all
disp(' ')
disp('---------------------------------------------------------');
disp('Now let''s use the pSPIEL algorithm to trade off informativeness and travelling cost')
disp(' ');
do_plot = 1;
addpath ../../SFO/sfo
addpath ../../maps
addpath ../../gridmapping
addpath ../../infoGain
addpath ../../semanticClassificationModel
addpath ../../explorationGridRepresentation
addpath ../../cameraModels
addpath ../cameraBasedSemanticGridRepresentation/utils
addpath ./maps
addpath ./utils
addpath ./incremental_set_evaluation
addpath ./utils/temp
addpath ../rhcp/utils
addpath ../informationgain/disp_utils
addpath ../../normalPathLibrary
%% getting map
grid_params = get_grid_params('semantic_camera_exploration_grid');
[ world_map, belief_map ] = get_map_obj( 'buildings.png', grid_params );
world_map.data = 1-world_map.data;

%% getting sensor params
camera_params = GetCameraParams('circular_120');


%% making nodes
bin_size = 200;
max_cell = 1000;
[X,Y] = meshgrid(bin_size/2:bin_size:max_cell,bin_size/2:bin_size:max_cell);
pose = [X(:),Y(:),60*ones(size(X(:)))];
%pose1 = pose + (bin_size/4-1)*rand(size(pose));
%pose2 = pose + (bin_size/4-1)*rand(size(pose));
%pose = [pose1;pose2;pose];
%pose(:,3) = 60*ones(size(pose(:,3)));
V = [1:size(pose,1)]';
D = zeros(size(V,1));
B = 9000;
R = bin_size;
Vroot = 1;
for i=1:size(pose,1)
    D(i,i) = 0;
 for j=i+1:size(pose,1)
    distance =  pose(i,:) - pose(j,:);
    distance = sqrt(sum(distance.*distance));
    D(i,j) = distance;
    D(j,i) = distance;
 end
end

%F = @(x) GetInformationGainIncremental(x, pose, belief_map.data, belief_map.data, world_map, camera_params );
%sub_test_fn = @(x) binary_binning_submodular( x,pose,bin_size);
%F = sfo_fn_wrapper(info_gain_fn);
%F = sfo_fn_wrapper(sub_test_fn);
F = @(x) binary_binning_submodular( x,pose,bin_size);

disp('[A E result] = sfo_pspiel_orienteering(F,V,B,D,maxIter,R)');
maxIter = 15;
params.num_nodes_per_bin = 3;
bin_size = 200;
pd = get_padded_decomposition( pose,bin_size,'binning',params);
[A E result] = sfo_pspiel_orienteering_fixed_pd(F,V,B,D,maxIter,Vroot,pd); 
%[A E result] = sfo_pspiel_orienteering(F,V,B,D,maxIter,Vroot);

%utility_pspiel = F(AP);

%[cost_pspiel edges_pspiel steiner_pspiel]= sfo_pspiel_get_cost(AP,D);

% if do_plot     
%     subplot(211)
%     sfo_plot_subgraph(merced_data.coords,edges_greedy,steiner_greedy)
%     title('Greedy-connect');
%     subplot(212)
%     sfo_plot_subgraph(merced_data.coords,edges_pspiel,steiner_pspiel)
%     title('pSPIEL');
% end

%disp(sprintf('Greedy-connect: Utility = %f, Cost = %f. pSPIEL: Utility =
%%f, Cost = %f.',utility_greedy,cost_greedy,utility_pspiel,cost_pspiel));
