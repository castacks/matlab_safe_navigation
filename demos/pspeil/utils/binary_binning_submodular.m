function [ cntr ] = binary_binning_submodular( V,pose,bin_size)
%BINARY_BINNING Summary of this function goes here
%   Detailed explanation goes here
% we will get n^2
if size(V,1)==1
    V = V';
end
disp('Query:')
disp(V)
q = pose(V,:);
q = q(:,1:2);
step_size = bin_size;
q = q/step_size;
q = ceil(q);
max_xy = [max(q(:,1)), max(q(:,2))];
cntr = 0;
for i=1:max_xy(1)
    for j=1:max_xy(2)
        if(~isempty(find(q(:,1)==i & q(:,2)==j))>0)
            if i==500 && j==500
                cntr = cntr+10;
            else
                cntr = cntr+1;
            end
        end
    end
end

end

