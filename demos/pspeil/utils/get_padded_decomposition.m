function [ pd ] = get_padded_decomposition( pose,r,method,params )
%GET_PADDED_DECOMPOSITION Summary of this function goes here
%   Detailed explanation goes here
%
% Assuming a uniform sampling between min and max

boxes.x = [];
boxes.y = [];
pd = {};

if isequal(method, '2D_uniform')
    max_pose = max(pose);
    min_pose = min(pose);
    min_box_x = min_pose(1):r:max_pose(1);    
    min_box_y = min_pose(2):r:max_pose(2);    
    
    cntr = 1;
    for i = 1:2:length(min_box_x)-1
        for j = 1:2:length(min_box_y)-1
            boxes(cntr).x = [min_box_x(i) min_box_x(i+1)]; 
            boxes(cntr).y = [min_box_y(j) min_box_y(j+1)];
            cntr = cntr+1;
        end
    end

elseif isequal(method, 'binning')
    n = params.num_nodes_per_bin;    
    q = ceil(pose/r);    
    max_xy = max(q);    
    cell = 1;
    for i=1:max_xy(1)
        for j=1:max_xy(2)
            I = find(q(:,1)==i & q(:,2)==j);
            ni = min(length(I),n);
            I=I(1:ni);
            pd{cell} = I;
            cell = cell+1;
        end
    end
    return;
end

for i=1:length(boxes)
    x = boxes(i).x;
    y = boxes(i).y;
    pd{i} = find(pose(:,1)>=x(1) & pose(:,1)<x(2) & pose(:,2)>=y(1) & pose(:,2)<y(2));
end

return