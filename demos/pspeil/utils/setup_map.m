function [ belief_map ] = setup_map( bounding_box, ground_truth_log_odds, belief_map, map_params)
%SETUP_MAP Summary of this function goes here
%   Detailed explanation goes here
for i =1:length(bounding_box)
    coords =  [bounding_box(i).min_x bounding_box(i).min_y;bounding_box(i).max_x bounding_box(i).max_y];
    ID = coord2index(coords(:,1),coords(:,2),map_params.scale,map_params.min(1),map_params.min(2));    
    [ ID, ~ ] = ProjectOnMap(ID,size(ground_truth_log_odds));
    [X,Y] = meshgrid(ID(1,1):ID(2,1),ID(1,2):ID(2,2));
    IND = sub2ind(size(belief_map),X,Y);
    if isequal(bounding_box(i).mode,'gt')
        belief_map(IND(:)) = ground_truth_log_odds(IND(:));
    else
        belief_map(IND(:)) = bounding_box(i).value;
    end
end

