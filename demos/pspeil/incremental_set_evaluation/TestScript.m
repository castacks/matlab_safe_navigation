clear all
x = [1:10]';
initial_conditions = 0;
[ value ] = IncrementalSetEvaluation(x,initial_conditions );
disp(value)

disp('Lets do the standard last element change')

x(10) = 11;
[ value ] = IncrementalSetEvaluation(x,initial_conditions );
disp(value)

disp('Lets increase the list')

x(11) = 11;
[ value ] = IncrementalSetEvaluation(x,initial_conditions );
disp(value)

disp('Lets do a middle element change')

x(4) = 11;
[ value ] = IncrementalSetEvaluation(x,initial_conditions );
disp(value)

disp('Lets do a no element change but initial conditions')

initial_conditions = 100;
[ value ] = IncrementalSetEvaluation(x,initial_conditions );
disp(value)

disp('Lets do the standard last element change')
x(end) = 12;
[ value ] = IncrementalSetEvaluation(x,initial_conditions );
disp(value)

disp('Lets do the standard last element change and initial conditions')

x(end) = 13;
initial_conditions = 1000;
[ value ] = IncrementalSetEvaluation(x,initial_conditions );
disp(value)