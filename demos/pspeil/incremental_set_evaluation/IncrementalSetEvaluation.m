function [ value ] = IncrementalSetEvaluation(x,initial_conditions )
%INCREMENTALSETEVALUATION Summary of this function goes here
%   Detailed explanation goes here

persistent test_function_last_evaluation;

if isempty(test_function_last_evaluation)
    test_function_last_evaluation.last_set_evaluated = [];
    test_function_last_evaluation.initial_conditions = initial_conditions;
    test_function_last_evaluation.values = [];
    recomputation_index = 1;
end


% Have initial conditions changed?
if isequal(initial_conditions,test_function_last_evaluation.initial_conditions(1))
   % find how much of the previous calculation can we use
    for i=1:size(test_function_last_evaluation.last_set_evaluated,1)
        if i>size(x,1)
            recomputation_index = max(i,1);
            break;
        end
        if test_function_last_evaluation.last_set_evaluated(i) ~= x(i)            
            recomputation_index = i;
            break;    
        else
            recomputation_index = i+1; % this saves us from an edge case
        end
    end
else
    recomputation_index = 1;
    test_function_last_evaluation.initial_conditions = initial_conditions;
end
    

if recomputation_index > size(x,1)
    value = test_function_last_evaluation.values(size(x,1));
    return;
end

test_function_last_evaluation.last_set_evaluated = x;
test_function_last_evaluation.initial_conditions(recomputation_index+1:end,:) = [];
test_function_last_evaluation.values(recomputation_index:end,:) = [];    
x = x(recomputation_index:end,:);


fprintf('Input Set:%i\n',size(x,1))
[ value, incremental_conditions ] = EvaluateSet( x, test_function_last_evaluation.initial_conditions(end,:) );
test_function_last_evaluation.initial_conditions(recomputation_index:end,:) = [];

test_function_last_evaluation.values = [test_function_last_evaluation.values;
                                        incremental_conditions.values];
test_function_last_evaluation.initial_conditions = [test_function_last_evaluation.initial_conditions;
                                                    incremental_conditions.initial_conditions];

return;

end

