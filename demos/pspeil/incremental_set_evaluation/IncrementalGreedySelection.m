function [greedy_selection  score root_score fixed_set_score ] = IncrementalGreedySelection(F, V, root, FixedSet, stopping_threshold)
%INCREMENTALGREEDYSELECTION Summary of this function goes here
%   Detailed explanation goes here
if size(V,1)==1
    V = V';
end

greedy_selection = [];
score = 0;
root_score = 0;

if ~isempty(root)
    root_score =F(root);
end

FixedSet = [root;FixedSet];

fixed_set_score = F(FixedSet);

temp_score = 100000000*ones(size(V));

while(1)    
    
    if isempty(V)
        return;
    end
    %% lazy evaluation    
    while(1)        
        [temp_score,I] = sort(temp_score,'descend'); % This step can be made faster   
        V = V(I);
        max_marginal_gain = temp_score(1) - fixed_set_score;
        if max_marginal_gain <= stopping_threshold
            return;
        end
        
        % step 1 get the maximum value
        QuerySet = [FixedSet;V(1)];
        
        temp_score(1) = F(QuerySet);
        % step2 test it, see if its better than second maximum
        if length(temp_score) <=1            
            break;
        end        

        if temp_score(1) >= temp_score(2)
            max_marginal_gain = temp_score(1) - fixed_set_score;
            break;
        end        
    end    
    
    if max_marginal_gain <= stopping_threshold
        return;
    end
    
    greedy_selection = [greedy_selection;V(1)];
    FixedSet = [FixedSet;V(1)];
    score(size(greedy_selection,1)) = temp_score(1);    
    V(1) = [];
    temp_score(1) = [];
end

