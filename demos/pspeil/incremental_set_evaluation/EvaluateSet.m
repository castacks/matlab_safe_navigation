function [ value, incremental_conditions ] = EvaluateSet( x, initial_value )
%EVALUATESET Summary of this function goes here
%   Detailed explanation goes here
value = initial_value;
incremental_conditions.values = zeros(size(x,1),1);
incremental_conditions.initial_conditions = zeros(size(x,1),1);
    for i=1:size(x,1) 
        incremental_conditions.initial_conditions(i,1) = value;
        value = value + 1;  
        incremental_conditions.values(i,1) = value;
        disp('Iteration')
    end
    incremental_conditions.initial_conditions(i+1,1) = value;
end

