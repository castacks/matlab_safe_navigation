function [ camera_params ] = GetCameraParams( id )
%GETCAMERAMODELS Summary of this function goes here
%   Detailed explanation goes here
camera_params = struct();

switch id
    case 'circular_120'
        camera_params.fov = deg2rad(120);
    case 'circular_20'
        camera_params.fov = deg2rad(20);
    otherwise
        error('Camera parameters requested not available')
end

end

