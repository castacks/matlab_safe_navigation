clear all
addpath ../../maps
addpath ../../gridmapping
addpath ../../infoGain
addpath ../../semanticClassificationModel
addpath ../../explorationGridRepresentation
addpath ../../cameraModels
addpath ./utils
addpath ./maps
addpath ../rhcp/utils
addpath ../informationgain/disp_utils
addpath ../../normalPathLibrary

%% getting map
grid_params = get_grid_params('semantic_camera_exploration_grid');
[ world_map, belief_map ] = get_map_obj( 'buildings.png', grid_params );
world_map.data = 1-world_map.data;

%% getting sensor params
camera_params = GetCameraParams('circular_120');


%% getting initial pose
pose = [200,200,60];

s.x = pose(1); s.y = pose(2); s.z = pose(3); s.psi = 0;

%% Get display object
[ display_obj ] = get_info_disp_obj( world_map, belief_map, [], s );
while(1)       
   % calculating information gain 
   info_gain = GetInformationGain( pose, world_map.data, belief_map.data, world_map, camera_params )
   % sensor simulation
   [ obs_coords, meas, range] = SimulateMeasurements( pose, world_map.data, world_map, camera_params );
   % update belief
   belief_map.data = UpdateBelief(obs_coords,meas,range,belief_map.data,world_map);
   % move 
   display_obj = update_info_disp_obj(world_map, belief_map, [], s, [], [], [],display_obj);
   pose = pose + [5,5,0];
   s.x = pose(1); s.y = pose(2); s.z = pose(3); s.psi = 0;
   
   pause(0.1)
   
end