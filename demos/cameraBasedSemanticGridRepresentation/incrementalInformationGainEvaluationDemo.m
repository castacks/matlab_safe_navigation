clear all
addpath ../../maps
addpath ../../gridmapping
addpath ../../infoGain
addpath ../../semanticClassificationModel
addpath ../../explorationGridRepresentation
addpath ../../cameraModels
addpath ./utils
addpath ./maps
addpath ../rhcp/utils
addpath ../informationgain/disp_utils
addpath ../../normalPathLibrary

%% getting map
grid_params = get_grid_params('semantic_camera_exploration_grid');
[ world_map, belief_map ] = get_map_obj( 'buildings.png', grid_params );
world_map.data = 1-world_map.data;

%% getting sensor params
camera_params = GetCameraParams('circular_120');


%% getting initial pose and making the nodes
pose = [200,200,60];
%increment_nodes = [20:20:200;20:20:200;zeros(1,10)]';
increment_nodes = [zeros(10,1),zeros(10,1),zeros(10,1)];
pose = bsxfun(@plus,pose,increment_nodes);
V = [1:size(pose,1)]';


x = [1:size(pose,1)-2]';

[ value , last_evaluation ] = GetInformationGainIncremental(x, pose, world_map.data, belief_map.data, world_map, camera_params );
disp(value)

disp('Lets do the same query again')

[ value , last_evaluation ] = GetInformationGainIncremental(x, pose, world_map.data, belief_map.data, world_map, camera_params );
disp(value)


disp('Lets do the standard last element change')

x(end) = size(pose,1)-1;
[ value , last_evaluation ] = GetInformationGainIncremental(x, pose, world_map.data, belief_map.data, world_map, camera_params );
disp(value)


disp('Lets increase the list')

x(end+1) = size(pose,1);
[ value , last_evaluation ] = GetInformationGainIncremental(x, pose, world_map.data, belief_map.data, world_map, camera_params );
disp(value)

disp('Lets do a middle element change')

x(ceil(size(x,1)/2)) = 1;
[ value , last_evaluation ] = GetInformationGainIncremental(x, pose, world_map.data, belief_map.data, world_map, camera_params );
disp(value)

disp('Lets do a no element change but initial conditions')

b_m.data = world_map.data;
[ value , last_evaluation ] = GetInformationGainIncremental(x, pose, world_map.data, b_m.data, world_map, camera_params );
disp(value)

disp('Lets do the standard last element change')
x(end) = 1;
[ value , last_evaluation ] = GetInformationGainIncremental(x, pose, world_map.data, b_m.data, world_map, camera_params );
disp(value)

disp('Lets do the standard last element change and initial conditions')

x(end) = 2;
initial_conditions = 1000;
[ value , last_evaluation ] = GetInformationGainIncremental(x, pose, world_map.data, belief_map.data, world_map, camera_params );
disp(value)