function laser_params = get_laser_params( id )
%GET_LASER_PARAMS Summary of this function goes here
%   Detailed explanation goes here

laser_params = struct();

switch id
    case 1
        laser_params.max_range = 150;                               %laser range
        laser_params.range_resolution = 0.8;                        %range resolution
        laser_params.laser_resolution = 1/laser_params.max_range;   %angle resolution
        laser_params.laser_fov = [-1,1];                            %field of view
    case 2
        laser_params.max_range = 120;                               %laser range
        laser_params.range_resolution = 0.8;                        %range resolution
        laser_params.laser_resolution = 1/laser_params.max_range;   %angle resolution
        laser_params.laser_fov = [-0.7,0.7];                        %field of view
    case 'circular_laser'
        laser_params.max_range = 50;                                %laser range
        laser_params.range_resolution = 0.8;                        %range resolution
        laser_params.laser_resolution = 1/laser_params.max_range;   %angle resolution
        laser_params.laser_fov = [-3.14,3.14];                      %field of view
    otherwise
        error('Laser parameters requested not available')
end

end

