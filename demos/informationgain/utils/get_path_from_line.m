function [ path ] = get_path_from_line( start_point, end_point, velocity, planning_params)
%GETPATHFROMLINE Summary of this function goes here
%   Detailed explanation goes here
path = struct();
path.x = [];
path.y = [];
path.psi = [];
line_dir = end_point - start_point;
line_dir = line_dir/sqrt(sum(line_dir.*line_dir));
heading = atan2(line_dir(2),line_dir(1));

path_time = planning_params.path_time;
num_elements = round(planning_params.path_time/planning_params.path_resolution_time);
path_length = (1:num_elements)*planning_params.path_resolution_time*velocity;

path.x = start_point(1) + path_length*line_dir(1);
path.y = start_point(2) + path_length*line_dir(2);
path.psi = ones(size(path_length))*heading;
end