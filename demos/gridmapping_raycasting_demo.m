% creating the environment and parameters for occupancy grid mapping
clear all
addpath ../gridmapping
addpath ../rayCasting

global p_occ_default ;
global p_empty_default;

global p_occ_sensor;
global p_empty_sensor ; 
global log_odds_occ; 
global log_odds_occ_sensor;
global log_odds_empty_sensor;

%% grid parameters
 p_occ_default = 0.5; %this can be changed
 p_empty_default = 1-p_occ_default; %this can be changed

 p_occ_sensor = 0.95; %this can be changed
 p_empty_sensor = 0.85; %this can be changed


%% Laser Parameters
max_range = 99; %this can be changed
range_resolution = 0.9;
laser_resolution = 1/max_range;
laser_fov = [-1,1];



%% Map Parameters
map_size = [201,201]; %this can be changed
obstacle_list = [100,110;99,110;101,110]; %this can be changed



%% Pre-Calculating grid update numbers
log_odds_occ = log(p_occ_default/p_empty_default);
log_odds_occ_sensor = log(p_occ_sensor/(1-p_occ_sensor));
log_odds_empty_sensor = log((1-p_empty_sensor)/p_empty_sensor);



%% Initializing maps and grids
robo_map = log_odds_occ*ones(map_size);  
world_map = zeros(map_size);
lin_ind =  sub2ind(size(world_map),obstacle_list(:,1),obstacle_list(:,2));
world_map(lin_ind) = 1;
clear lin_ind

world_map = 1 - getMapfromImage('../maps/example_map.tif');
robo_map = log_odds_occ*ones(size(world_map));  

%% Starting Robo Pose 
robo_pos = [100,100];
robo_yaw = deg2rad(90);



%% running ray tracing and occupancy grid mapping
for i=0:10
[x,y,r,collision] = fast_trace(world_map,laser_fov, laser_resolution, max_range, range_resolution, robo_pos,robo_yaw);
robo_map = update_robo_world(laser_fov,laser_resolution,range_resolution,x,y,r,collision,robo_pos,robo_yaw,robo_map);
% grid mapping % robo map is grid map
%     for j= 1:360
%         info_gain(i+1,j) = estimate_info_gain(robo_map,deg2rad(j),max_range,[100,100]); 
%     end
end
%% displaying results
imshow( 1-world_map)
hold on
plot([100,y(1)],[100,x(1)],'c')
hold off
figure
imshow(robo_map - min(min(robo_map)))

clear i
clear j