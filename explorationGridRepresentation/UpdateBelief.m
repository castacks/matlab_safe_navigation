function [ belief_map ] = UpdateBelief(coords,meas,range,belief_map,map_struct)
%UPDATEBELIEF Summary of this function goes here
%   Detailed explanation goes here
[values, valid] = GetMapValues(coords, belief_map, map_struct);
coords = coords(valid>0,:);
values = values(valid>0,:);
range = range(valid>0,:);
meas = meas(valid>0,:);

[p_i,p_u] = GetMeasurementProbability(meas,range);
log_odds = UpdateLogOdds( p_i, p_u, values);
belief_map = UpdateCellsCoord( log_odds, belief_map, coords, map_struct);
end