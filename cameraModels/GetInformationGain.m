function [ info_gain, incremental_conditions ] = GetInformationGain( pose, world_map, belief_map, map_struct, sensor_params, base_belief_map )
%GETINFORMATIONGAIN Summary of this function goes here
%   Detailed explanation goes here
incremental_conditions.info_gain = [];
incremental_conditions.initial_conditions.belief_map = belief_map;
incremental_conditions.initial_conditions(size(pose,1),1).belief_map = belief_map;

updated_belief_map = belief_map;
if ~isempty(base_belief_map)
    old_ent = EntropyLogOdds(belief_map);
else
    old_ent = EntropyLogOdds(base_belief_map);
end

for i=1:size(pose,1)    
    [ obs_coords, meas, range] = SimulateMeasurements( pose(i,:), world_map, map_struct, sensor_params );
    updated_belief_map = UpdateBelief(obs_coords,meas,range,updated_belief_map,map_struct);    
    incremental_conditions.initial_conditions(i+1,1).belief_map = updated_belief_map;        
    values_new_ent = EntropyLogOdds(updated_belief_map);
    values_new_ent(values_new_ent>old_ent)=old_ent(values_new_ent>old_ent);
    info_gain = sum(old_ent(:) - values_new_ent(:));
    incremental_conditions.info_gain(i,1) = info_gain;
end


end