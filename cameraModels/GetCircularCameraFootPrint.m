function [ X,Y ] = GetCircularCameraFootPrint( center, height, cone_angle, map_struct)
foot_print_rad = height*tan(cone_angle*0.5);
bounding_box = [center(1)-foot_print_rad,center(2)-foot_print_rad;center(1)+foot_print_rad,center(2)+foot_print_rad];
[X,Y] = meshgrid(bounding_box(1,1):map_struct.scale:bounding_box(2,1),bounding_box(1,2):map_struct.scale:bounding_box(2,2));
X = X(:); Y = Y(:);


Xc = X - center(1);
Yc = Y - center(2);
r = Xc.*Xc + Yc.*Yc;
rr = foot_print_rad*foot_print_rad;
X(r>rr) = [];
Y(r>rr) = [];

end


