function [ obs_coords, meas, range] = SimulateMeasurements( pose, world_map, map_struct, sensor_params )
%SIMULATEMEASUREMENTS Summary of this function goes here
%   Detailed explanation goes here
[ X,Y ] = GetCircularCameraFootPrint(pose(1,1:2), pose(1,3), sensor_params.fov, map_struct);
obs_coords = [X,Y];
obs_coords = [obs_coords,zeros(size(obs_coords,1),1)];
range = bsxfun(@minus,obs_coords,pose(1,1:3));
range = sum(range.*range,2);
range = sqrt(range);

[values, valid] = GetMapValues(obs_coords, world_map, map_struct);
obs_coords = obs_coords(valid>0,:);
values = values(valid>0,:);
values(values>0,:) =1;
values(values<=0,:) =0;

range = range(valid>0,:);

p_ground_truth = ones(size(values));
%p_ground_truth(values==1) = GetInterestingGivenInterestingProbability( range(values==1) );
%p_ground_truth(values==0) = GetUnInterestinGivenUninterestingProbability( range(values==0) );
meas = GetBinaryStochasticMeasurements( values, p_ground_truth);
end
