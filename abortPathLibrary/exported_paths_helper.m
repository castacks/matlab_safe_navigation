clear
load('exported_maneuvers.mat');

%paths stores all the emergency maneuvers as a matrix.
% each path starts at time = 0
% what each column represents is captured in trajectoryMatrixColIds

%Example -- Plotting the x and y of all the paths

plot(paths(:,trajectoryMatrixColIds.x),paths(:,trajectoryMatrixColIds.y),'.')
axis equal