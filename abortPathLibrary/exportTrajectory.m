function [ paths,trajectoryMatrixColIds] = exportTrajectory( selectedPathsIds, pathLookUpId, pathList )
%EXPORTTRAJECTORY Summary of this function goes here
%   Detailed explanation goes here
trajectoryMatrixColIds = struct;
trajectoryMatrixColIds.time = 1;
trajectoryMatrixColIds.x = 2;
trajectoryMatrixColIds.y = 3;
trajectoryMatrixColIds.z = 4;
trajectoryMatrixColIds.heading = 5;
trajectoryMatrixColIds.roll = 6;
paths = [];
for i=1:size(selectedPathsIds,1)
    ids = pathLookUpId(selectedPathsIds(i),:);
    paths = [paths;pathList(ids(1):ids(2),:)];
end

