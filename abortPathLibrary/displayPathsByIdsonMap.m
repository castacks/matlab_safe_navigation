function [handleSet] = displayPathsByIdsonMap(pose,globalPathList,globalPathIds,validIds,resolution,minx,miny,c)
%DISPLAYMAPVELOCITYPATHS Summary of this function goes here
%   Detailed explanation goes here
hold on
handleSet = zeros(size(validIds));
    for i=1:size(validIds,1)
        ids = globalPathIds(validIds(i),:);
        pathxy = globalPathList(ids(1):ids(2),2:3);
        pathxy = transformPoint2Global(pathxy,pose(3),pose(1:2));
        pathxy = coord2indexNotRound(pathxy(:,1),pathxy(:,2),resolution,minx,miny);
        handleSet(i) = plot(pathxy(:,1),pathxy(:,2),c);
    end
end
